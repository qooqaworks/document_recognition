from bs4 import BeautifulSoup

from .fiscal_code_utilities import check_number, get_date_of_birth_and_sex, get_municipality_and_province
from .general_utils import get_block_coordinates


def h_insurance_back_parser(file_path: str) -> dict:
    with open(file_path, 'r', encoding='utf-8') as f:
        document = f.read()
    soup = BeautifulSoup(document, 'lxml-xml')
    page_size = soup.find('div', attrs={'id': 'page_1'})
    page_size = get_block_coordinates(page_size, False)
    lines = soup.find_all('span', attrs={'class': 'ocr_line'})
    data = dict(surname='', name='', birth_date='', fiscal_code='', expiration_date='')
    for line in lines:
        line_size = get_block_coordinates(line)
        top_x_rel = (line_size['top_x'] / page_size['bottom_x']) * 100
        words = line.find_all('span', attrs={'class': 'ocrx_word'})
        if line_size['top_y']/page_size['bottom_y'] * 100 >= 55:
            words = line.find_all('span', attrs={'class': 'ocrx_word'})
            top_y_rel = (line_size['top_y']/page_size['bottom_y']) * 100
            if 55 <= top_y_rel < 65:
                data['surname'] = ' '.join([w.text for w in words])
            elif 65 <= top_y_rel < 73:
                for word in words:
                    word_size = get_block_coordinates(word)
                    if (word_size['bottom_x'] / page_size['bottom_x']) * 100 >= 85 and not data['birth_date']:
                        data['birth_date'] = word.text
                    else:
                        data['name'] = '{} {}'.format(data['name'], word.text) if data['name'] else word.text
            elif 73 <= top_y_rel <= 80:
                fcode = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    top_x_rel = (word_size['top_x']/page_size['bottom_x']) * 100
                    if 1 <= top_x_rel <= 15:
                        if check_number(word.text):
                            data['fiscal_code'] = word.text
                            break
                        else:
                            fcode.append(word.text)
                if not data['fiscal_code']:
                    data['fiscal_code'] = ''.join(fcode)
            elif 83 <= top_y_rel:
                # print('here', words)
                exp_date = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    # print('!!!!', word.text)
                    if (word_size['bottom_x'] / page_size['bottom_x']) * 100 >= 85:
                        exp_date.append(word.text)
                if not data['expiration_date']:
                    data['expiration_date'] = ''.join(exp_date)
        else:
            pass
            # print(words)
    return data


def h_insurance_front_parser(file_path: str) -> dict:
    with open(file_path, 'r', encoding='utf-8') as f:
        document = f.read()
    soup = BeautifulSoup(document, 'lxml-xml')
    page_size = soup.find('div', attrs={'id': 'page_1'})
    page_size = get_block_coordinates(page_size, False)
    fiscal_code_correct = False
    lines = soup.find_all('span', attrs={'class': 'ocr_line'})
    data = dict(last_name='', first_name='', birth_date='', fiscal_code='', expiration_date='',
                municipality='', province='', sex='')
    for line in lines:
        line_size = get_block_coordinates(line)
        top_x_rel = (line_size['top_x'] / page_size['bottom_x']) * 100
        top_y_rel = (line_size['top_y']/page_size['bottom_y']) * 100
        words = line.find_all('span', attrs={'class': 'ocrx_word'})
        if 35 <= top_x_rel <= 46 and top_y_rel < 43:
            fiscal_code = list()
            for word in words:
                word_size = get_block_coordinates(word)
                word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                if word_top_x_rel <= 78:
                    if len(word.text) == 16:
                        fiscal_code = [word.text,]
                        break
                    else:
                        fiscal_code.append(word.text)
            fiscal_code = ''.join(fiscal_code)
            if check_number(fiscal_code):
                data.update(get_date_of_birth_and_sex(fiscal_code))
                data.update(get_municipality_and_province(fiscal_code))
                data['fiscal_code'] = fiscal_code
                fiscal_code_correct = True
            else:
                data['fiscal_code'] = fiscal_code
        elif 40 <= top_x_rel <= 45 and top_y_rel < 70:
            if 43 <= top_y_rel <= 50:
                last_name = [word.text for word in words]
                data['last_name'] = ' '.join(last_name)
            elif 50 < top_y_rel <= 59:
                first_name = [word.text for word in words]
                data['first_name'] = ' '.join(first_name)
            elif 59 < top_y_rel <= 72 and not fiscal_code_correct:
                municipality = [word.text for word in words]
                data['municipality'] = ' '.join(municipality)
        elif 3 < top_x_rel <= 13 and 66 < top_y_rel <= 78.5:
            expiration_date = list()
            province = list()
            for word in words:
                word_size = get_block_coordinates(word)
                word_bottom_x_rel = (word_size['bottom_x'] / page_size['bottom_x']) * 100
                word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                if word_bottom_x_rel <= 30:
                    expiration_date.append(word.text)
                elif 40 < word_top_x_rel < 45 and not fiscal_code_correct:
                    province.append(word.text)
            data['expiration_date'] = '/'.join(expiration_date)
            if not fiscal_code_correct:
                data['province'] = ''.join(province)
        elif 36 <= top_x_rel <= 46 and 75.5 < top_y_rel <= 90:
            date_of_birth = list()
            for word in words:
                word_size = get_block_coordinates(word)
                word_bottom_x_rel = (word_size['bottom_x'] / page_size['bottom_x']) * 100
                if 55 <= word_bottom_x_rel <= 64:
                    date_of_birth.append(word.text)
            if not fiscal_code_correct:
                data['birth_date'] = '/'.join(date_of_birth)
    return data




