import os

from bs4 import BeautifulSoup

from .general_utils import get_block_coordinates


def patent_front_parser(file_path: str) -> dict:
    with open(file_path, 'r', encoding='utf-8') as f:
        document = f.read()
    soup = BeautifulSoup(document, 'lxml-xml')
    page_size = soup.find('div', attrs={'id': 'page_1'})
    page_size = get_block_coordinates(page_size, False)
    lines = soup.find_all('span', attrs={'class': 'ocr_line'})
    data = dict(last_name='', first_name='', birth_date='', issue_date='', expiration_date='',
                municipality='', province='', document_number='', office='', patent_type='')
    for line in lines:
        line_size = get_block_coordinates(line)
        top_x_rel = (line_size['top_x'] / page_size['bottom_x']) * 100
        top_y_rel = (line_size['top_y'] / page_size['bottom_y']) * 100
        if top_x_rel >= 20:
            words = line.find_all('span', attrs={'class': 'ocrx_word'})
            if 11.5 <= top_y_rel <= 16.5:
                last_name = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                    if word_top_x_rel >= 31.5:
                        last_name.append(word.text)
                if not data['last_name'] and last_name:
                    data['last_name'] = ' '.join(last_name)
            elif 16.5 < top_y_rel <= 24.5:
                name = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                    if word_top_x_rel >= 31.5:
                        name.append(word.text)
                if not data['first_name'] and name:
                    data['first_name'] = ' '.join(name)
            elif 24.5 < top_y_rel <= 30.5:
                region = list()
                birth_date = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    word_bottom_x_rel = (word_size['bottom_x'] / page_size['bottom_x']) * 100
                    if 35 < word_bottom_x_rel <= 50:
                        birth_date.append(word.text)
                    elif 35 < word_bottom_x_rel:
                        region.append(word.text)
                if region and len(region[-1]) >= 2:
                    data['province'] = region[-1].replace('(', '').replace(')', '')
                data['municipality'] = ' '.join(region[:-1])
                data['birth_date'] = '/'.join(birth_date)
            elif 30.5 < top_y_rel <= 36.5:
                date_issued = list()
                authority = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    word_bottom_x_rel = (word_size['bottom_x'] / page_size['bottom_x']) * 100
                    word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                    if 35 < word_bottom_x_rel <= 53:
                        date_issued.append(word.text)
                    elif word_top_x_rel >= 53:
                        authority.append(word.text)
                data['issue_date'] = '/'.join(date_issued)
                data['office'] = ' '.join(authority)
            elif 36.5 < top_y_rel <= 43.5:
                expire_date = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    word_bottom_x_rel = (word_size['bottom_x'] / page_size['bottom_x']) * 100
                    if 35 < word_bottom_x_rel <= 55:
                        expire_date.append(word.text)
                data['expiration_date'] = '/'.join(expire_date)
            elif 44 < top_y_rel <= 50:
                number = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    word_bottom_x_rel = (word_size['bottom_x'] / page_size['bottom_x']) * 100
                    if 35 < word_bottom_x_rel <= 55:
                        number.append(word.text)
                data['document_number'] = ''.join(number)
    return data


def patent_back_parser(dir_path: str) -> dict:
    cat_mapper = {'1': 'AM',
                  '2': 'A1',
                  '3': 'A2',
                  '4': 'A',
                  '5': 'B1',
                  '6': 'B',
                  '7': 'C1',
                  '8': 'C',
                  '9': 'D1',
                  '10': 'D',
                  '11': 'BE',
                  '12': 'C1E',
                  '13': 'CE',
                  '14': 'D1E',
                  '15': 'DE'}
    result = {'AM': dict(start_date='', end_date=''),
              'A1': dict(start_date='', end_date=''),
              'A2': dict(start_date='', end_date=''),
              'A': dict(start_date='', end_date=''),
              'B1': dict(start_date='', end_date=''),
              'B': dict(start_date='', end_date=''),
              'C1': dict(start_date='', end_date=''),
              'C': dict(start_date='', end_date=''),
              'D1': dict(start_date='', end_date=''),
              'D': dict(start_date='', end_date=''),
              'BE': dict(start_date='', end_date=''),
              'C1E': dict(start_date='', end_date=''),
              'CE': dict(start_date='', end_date=''),
              'D1E': dict(start_date='', end_date=''),
              'DE': dict(start_date='', end_date='')}
    for entry in os.scandir(dir_path):
        if entry.is_file() and entry.name.endswith('.txt'):
            with open(entry.path, 'rt') as f:
                file_content = f.read()
                if file_content and len(file_content.strip()) > 6:
                    if entry.name.rsplit('.', 1)[0].split('_')[-1] == '1':
                        result[cat_mapper[entry.name.rsplit('.', 1)[0].split('_')[-2]]][
                            'start_date'] = file_content.strip()
                    elif entry.name.rsplit('.', 1)[0].split('_')[-1] == '2':
                        result[cat_mapper[entry.name.rsplit('.', 1)[0].split('_')[-2]]][
                            'end_date'] = file_content.strip()
    return result
