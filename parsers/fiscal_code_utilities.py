import calendar
#import locale
import sqlite3



ODD_CHAR_VALUE = {'0': 1, '1': 0, '2': 5, '3': 7, '4': 9, '5': 13, '6': 15, '7': 17, '8': 19,
                  '9': 21, 'A': 1, 'B': 0, 'C': 5, 'D': 7, 'E': 9, 'F': 13, 'G': 15, 'H': 17,
                  'I': 19, 'J': 21, 'K': 2, 'L': 4, 'M': 18, 'N': 20, 'O': 11, 'P': 3, 'Q': 6,
                  'R': 8, 'S': 12, 'T': 14, 'U': 16, 'V': 10, 'W': 22, 'X': 25, 'Y': 24, 'Z': 23}

EVEN_CHAR_VALUE = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
                   'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9,
                   'K': 10, 'L': 11, 'M': 12, 'N': 13, 'O': 14, 'P': 15, 'Q': 16, 'R': 17, 'S': 18,
                   'T': 19, 'U': 20, 'V': 21, 'W': 22, 'X': 23, 'Y': 24, 'Z': 25}

MODULO_CHECK = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F', 6: 'G', 7: 'H', 8: 'I', 9: 'J', 10: 'K', 11: 'L',
                12: 'M', 13: 'N', 14: 'O', 15: 'P', 16: 'Q', 17: 'R', 18: 'S', 19: 'T', 20: 'U', 21: 'V', 22: 'W',
                23: 'X', 24: 'Y', 25: 'Z'}

MONTH_LETTER = {'A': 'January', 'B': 'February', 'C': 'March', 'D': 'April', 'E': 'May', 'H': 'June', 'L': 'July',
                'M': 'August', 'P': 'September', 'R': 'October', 'S': 'November', 'T': 'December'}

MONTH_NUMBER_LETTER = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'H', 7: 'L', 8: 'M',
                       9: 'P', 10: 'R', 11: 'S', 12: 'T'}


def check_number(number: str):
    if len(number) == 16:
        total = 0
        number = number.upper()
        odds = number[:-1:2]
        even = number[1:-1:2]
        for o in odds:
            total += ODD_CHAR_VALUE[o]
        for e in even:
            total += EVEN_CHAR_VALUE[e]
        if number[-1] == MODULO_CHECK[total % 26]:
            return True
        else:
            return False
    else:
        return False


def translate_name_to_abbreviation(name: str) -> str:
    name = name.upper()
    vowels = ['A', 'E', 'I', 'O', 'U']
    if len(name) < 3:
        abbreviation = name.upper() + 'XXX'[:-len(name)]
    else:
        abbreviation = ''.join([l for l in name if l not in vowels])[:3]
        if len(abbreviation) < 3:
            abbreviation = abbreviation + ''.join([l for l in name if l in vowels])[:3-len(abbreviation)]
    return abbreviation


def translate_birth_date_to_code(birth_date: dict) -> str:
    day = int(birth_date['day']) if 'M' in birth_date['sex'].upper() else 40 + int(birth_date['day'])
    month = MONTH_NUMBER_LETTER[int(birth_date['month'])]
    return'{}{}{}'.format(birth_date['year'][-2:], month, day)


def get_date_of_birth_and_sex(code: str) -> dict:
    date = code[6:11].upper()
    #current_locale = locale.getdefaultlocale()[0] if locale.getlocale()[0] else 'en_US'
    #locale.setlocale(locale.LC_ALL, 'en_US')
    month = list(calendar.month_name).index(MONTH_LETTER[date[2]])
    #locale.setlocale(locale.LC_ALL, current_locale)
    year = date[:2]
    day = int(date[-2:])
    if day > 40:
        sex = 'F'
        day -= 40
    else:
        sex = 'M'
    return dict(birth_date='{}/{}/{}'.format(day, month, year), sex=sex)


def get_municipality_and_province(code) -> dict:
    municipality_code = code[-5:-1]
    conn = sqlite3.connect('./parsers/db.sqlite3')
    c = conn.cursor()
    c.execute('''SELECT * FROM states WHERE code="{code}"'''.format(code=municipality_code))
    result = c.fetchone()
    conn.close()
    data = dict()
    if result:
        data['municipality'] = result[2]
        data['province'] = result[3]
    return data
