from .h_insurance_parsers import h_insurance_back_parser, h_insurance_front_parser
from .patent_parsers import patent_front_parser, patent_back_parser
from .old_id_parsers import old_id_front_parser, old_id_back_parser
from .new_id_parsers import new_id_front_parser, new_id_back_parser


class Parser:
    """
    This class aggregates parsers for all types of documents.
    Must be initiated with recognized image (hOCR or TSV/text) path
    """

    def __init__(self, file_path):
        self.rec_file_path = file_path
        self.results = dict()

    def old_id_front(self):
        self.results.update(old_id_front_parser(self.rec_file_path))

    def old_id_back(self):
        self.results.update(old_id_back_parser(self.rec_file_path))

    def new_id_front(self):
        self.results.update(new_id_front_parser(self.rec_file_path))

    def new_id_back(self):
        self.results.update(new_id_back_parser(self.rec_file_path))

    def patent_front(self):
        self.results.update(patent_front_parser(self.rec_file_path))

    def patent_back(self):
        self.results.update(patent_back_parser(self.rec_file_path))

    def h_insurance_front(self):
        self.results.update(h_insurance_front_parser(self.rec_file_path))

    def h_insurance_back(self):
        self.results.update(h_insurance_back_parser(self.rec_file_path))
