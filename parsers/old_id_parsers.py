from bs4 import BeautifulSoup

from .general_utils import get_block_coordinates


def old_id_front_parser(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        document = f.read()
    soup = BeautifulSoup(document, 'lxml-xml')
    page_size = soup.find('div', attrs={'id': 'page_1'})
    page_size = get_block_coordinates(page_size, False)
    lines = soup.find_all('span', attrs={'class': 'ocr_line'})
    data = dict(last_name='', first_name='', birth_date='', birth_place='', nationality='',
                municipality='', province='', street_address='', street_number='')
    for line in lines:
        line_size = get_block_coordinates(line)
        top_x_rel = (line_size['top_x'] / page_size['bottom_x']) * 100
        top_y_rel = (line_size['top_y'] / page_size['bottom_y']) * 100
        words = line.find_all('span', attrs={'class': 'ocrx_word'})
        if top_x_rel < 50:
            if 8 <= top_y_rel <= 14:
                last_name = list()
                for word in words:
                    last_name.append(word.text)
                data['last_name'] = ' '.join(last_name)
            elif 14 < top_y_rel <= 18.5:
                name = list()
                for word in words:
                    name.append(word.text)
                data['first_name'] = ' '.join(name)
            elif 18.5 < top_y_rel <= 23.5:
                date = list()
                for word in words:
                    if len(word.text.split('-')) == 3:
                        date = [word.text]
                        break
                    else:
                        date.append(word.text)
                data['birth_date'] = '-'.join(date)
            elif 29 < top_y_rel <= 32.5:
                birth_place = list()
                for word in words:
                    birth_place.append(word.text)
                data['birth_place'] = ' '.join(birth_place)
            elif 33 < top_y_rel <= 37.5:
                nationality = list()
                for word in words:
                    nationality.append(word.text)
                data['nationality'] = ' '.join(nationality)
            elif 37.5 < top_y_rel <= 42.5:
                municipality = list()
                for word in words:
                    municipality.append(word.text)
                data['municipality'] = ' '.join(municipality[:-1])
                data['province'] = municipality[-1].replace('(', '').replace(')', '') if len(municipality[-1]) >= 2 else ''
            elif 42.5 < top_y_rel <= 47.5:
                street = list()
                for word in words:
                    street.append(word.text)
                data['street_address'] = ' '.join(street[:-1])
                data['street_number'] = street[-1]
    return data


def old_id_back_parser(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        file_content = f.read()
    data = dict(document_id='')

    if file_content:
        data['document_id'] = file_content.strip()

    return data
