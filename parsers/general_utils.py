from bs4 import BeautifulSoup


def get_block_coordinates(block: BeautifulSoup, internal_block: bool = True) -> dict:
    """
    hOCR blocks are formed differently, size information of page is located as second element in title tag,
    while all internal blocks stores this information as first element in same tag

    :param block: part of hOCR
    :param internal_block: switcher that identifies whether is needed to extract size of internal block or whole page.
    :return: dictionary with all coordinates of block
    """
    data = dict()
    if internal_block:
        block_size = block['title'].split(';')[0].strip()
    else:
        block_size = block['title'].split(';')[1].strip()
    block_size = block_size.split(' ')
    data['top_x'] = int(block_size[1])
    data['top_y'] = int(block_size[2])
    data['bottom_x'] = int(block_size[3])
    data['bottom_y'] = int(block_size[4])
    return data
