import os
import subprocess

from .parsers import Parser

TESS_DATA_DIR = "/home/denny_k/tesseract/tesseract/tessdata"

def process_image(documents: list) -> list:
    """
    This function runs tesseract and then applies parsers to get appropriate information

    :param image_path: path to image
    :param file_type: type of image
    :type image_path: str
    :type file_type: str
    :return: dictionary with all data
    :rtype: dict
    """

    for document in documents:
        file_type = document['type'].lower()
        document['ocr_data'] = dict()
        if file_type != 'patent_back' and file_type != 'old_id_back':
            image_path_wo_extension = '.'.join(document['path'].split('.')[:-1])
            subprocess.call('tesseract --tessdata-dir {2} {0} {1} -l ita hocr'.format(document['path'], image_path_wo_extension, TESS_DATA_DIR), shell=True)
            rec_path = image_path_wo_extension + '.hocr'
            parser = Parser(rec_path)
        elif file_type == 'patent_back':
            files = os.scandir(document['path'])
            for file in files:
                if file.name.endswith('.png'):
                    image_path_wo_extension = '.'.join(file.path.split('.')[:-1])
                    # print('tesseract --tessdata-dir {2} {0} {1} -l ita hocr'.format(file.path, image_path_wo_extension, TESS_DATA_DIR))
                    subprocess.call('tesseract --tessdata-dir {2} {0} {1} -l ita'.format(file.path, image_path_wo_extension, TESS_DATA_DIR), shell=True)
            rec_path = document['path']
            parser = Parser(rec_path)
        elif file_type == 'old_id_back':
            image_path_wo_extension = '.'.join(document['path'].split('.')[:-1])
            subprocess.call(
                'tesseract --tessdata-dir {2} {0} {1} -l ita txt'.format(document['path'], image_path_wo_extension,
                                                                          TESS_DATA_DIR), shell=True)
            rec_path = image_path_wo_extension + '.txt'
            parser = Parser(rec_path)
        if file_type == 'old_id_front':
            parser.old_id_front()
            document['ocr_data'].update(parser.results)
        elif file_type == 'old_id_back':
            parser.old_id_back()
            document['ocr_data'].update(parser.results)
        elif file_type == 'new_id_front':
            parser.new_id_front()
            document['ocr_data'].update(parser.results)
        elif file_type == 'new_id_back':
            parser.new_id_back()
            document['ocr_data'].update(parser.results)
        elif file_type == 'patent_front':
            parser.patent_front()
            document['ocr_data'].update(parser.results)
        elif file_type == 'patent_back':
            parser.patent_back()
            document['ocr_data'].update(parser.results)
        elif file_type == 'h_insurance_back':
            parser.h_insurance_back()
            document['ocr_data'].update(parser.results)
        elif file_type == 'h_insurance_front':
            parser.h_insurance_front()
            document['ocr_data'].update(parser.results)
    return documents
