import datetime
from bs4 import BeautifulSoup


from .fiscal_code_utilities import check_number, translate_birth_date_to_code, translate_name_to_abbreviation
from .general_utils import get_block_coordinates


def new_id_front_parser(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        document = f.read()
    soup = BeautifulSoup(document, 'lxml-xml')
    page_size = soup.find('div', attrs={'id': 'page_1'})
    page_size = get_block_coordinates(page_size, False)
    lines = soup.find_all('span', attrs={'class': 'ocr_line'})
    data = dict(last_name='', first_name='', birth_date='', issue_date='', expiration_date='', height='',
                municipality='', province='', document_number='', place_of_birth='', place_released='', nationality='')
    for line in lines:
        line_size = get_block_coordinates(line)
        top_x_rel = (line_size['top_x'] / page_size['bottom_x']) * 100
        top_y_rel = (line_size['top_y'] / page_size['bottom_y']) * 100
        words = line.find_all('span', attrs={'class': 'ocrx_word'})
        if 4 <= top_y_rel <= 16:
            document_number = list()
            for word in words:
                word_size = get_block_coordinates(word)
                if word_size['bottom_y'] - word_size['top_y'] <= 5:
                    continue
                word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                if 70 <= word_top_x_rel <= 94:
                    document_number.append(word.text)
            if not data['document_number']:
                data['document_number'] = ''.join(document_number).strip()

        elif 18 <= top_x_rel <= 27.5 and 23 <= top_y_rel <= 28:
            municipality = list()
            for word in words:
                municipality.append(word.text)
            if not data['place_released']:
                data['place_released'] = ' '.join(municipality)
        elif 31.9 <= top_x_rel <= 36.9 and top_y_rel < 57:
            if 32 <= top_y_rel <= 36.5:
                last_name = list()
                if not data['last_name']:
                    for word in words:
                        last_name.append(word.text)
                    data['last_name'] = ' '.join(last_name)
            elif 37 < top_y_rel <= 42:
                first_name = list()
                for word in words:
                    first_name.append(word.text)
                data['first_name'] = ' '.join(first_name)
            elif 49.5 <= top_y_rel <= 54:
                place_date_of_birth = list()
                for word in words:
                    place_date_of_birth.append(word.text)
                date = place_date_of_birth.pop(-1)
                if len(date) >= 8:
                    data['birth_date'] = date.replace(',', '.')
                    data['place_of_birth'] = ' '.join(place_date_of_birth)
                    province = place_date_of_birth.pop(-1).replace('(', '').replace(')', '')
                    data['province'] = province
                    data['municipality'] = ' '.join(place_date_of_birth)
                else:
                    i = 0
                    for w in reversed(place_date_of_birth):
                        i += 1
                        date = w + date
                        if len(date) >= 8:
                            data['birth_date'] = date.replace(',', '.')
                            break
                    province = place_date_of_birth[-1].replace('(', '').replace(')', '')
                    data['province'] = province
                    data['municipality'] = ' '.join(place_date_of_birth[:-1*(i-1)])
        elif 59 <= top_y_rel <= 64 and top_x_rel >= 31.9:
            sex = list()
            height = list()
            nationality = list()
            for word in words:
                word_size = get_block_coordinates(word)
                word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                if word_top_x_rel <= 36.9:
                    sex.append(word.text)
                elif 46 < word_top_x_rel <= 51.1:
                    height.append(word.text)
                elif 63 < word_top_x_rel <= 67:
                    nationality.append(word.text)
            data['sex'] = ''.join(sex)
            data['height'] = ''.join(height)
            data['nationality'] = ' '.join(nationality)
        elif 66.5 <= top_y_rel < 70 and top_x_rel >= 31.9:
            issue_date = list()
            expiry = list()
            for word in words:
                word_size = get_block_coordinates(word)
                word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                # print(word.text)
                if word_top_x_rel < 50:
                    issue_date.append(word.text)
                elif 63 < word_top_x_rel <= 80:
                    expiry.append(word.text)
            if not data['issue_date']:
                data['issue_date'] = '.'.join(issue_date)
            if not data['expiration_date']:
                data['expiration_date'] = '.'.join(expiry)
    return data


def new_id_back_parser(file_path):
    with open(file_path, 'r', encoding='utf-8') as f:
        document = f.read()
    soup = BeautifulSoup(document, 'lxml-xml')
    page_size = soup.find('div', attrs={'id': 'page_1'})
    page_size = get_block_coordinates(page_size, False)
    lines = soup.find_all('span', attrs={'class': 'ocr_line'})
    data = dict(fiscal_code='', address='', province='')
    check_data = dict()
    for line in lines:
        line_size = get_block_coordinates(line)
        top_x_rel = (line_size['top_x'] / page_size['bottom_x']) * 100
        top_y_rel = (line_size['top_y'] / page_size['bottom_y']) * 100
        words = line.find_all('span', attrs={'class': 'ocrx_word'})
        if top_x_rel <= 25:
            if 18 <= top_y_rel <= 21.5:
                fiscal_code = list()
                for word in words:
                    word_size = get_block_coordinates(word)
                    word_top_x_rel = (word_size['top_x'] / page_size['bottom_x']) * 100
                    if word_top_x_rel <= 26:
                        fiscal_code.append(word.text)
                data['fiscal_code'] = ''.join(fiscal_code)
            elif 28.3 <= top_y_rel <= 31.3:
                address = list()
                for word in words:
                    address.append(word.text)
                data['province'] = ''.join(w for w in address.pop(-1) if w.isalpha())
                data['address'] = ' '.join(address)

            elif not check_number(data['fiscal_code']):
                if 77 <= top_y_rel <= 85:
                    code = list()
                    for w in words:
                        code.append(w.text)
                    code = ''.join(code)
                    check_data['year'] = '20' + code[:2] if int(code[:2]) <= int(datetime.date.today().strftime('%y')) else '19' + code[:2]
                    check_data['month'] = code[2:4]
                    check_data['day'] = code[4:6]
                    check_data['sex'] = code[7]
                elif 85 < top_y_rel <= 94:
                    name = list()
                    for word in words:
                        name.append(word.text)
                    name = ''.join(name)
                    check_data['last_name'] = name.split('<<')[0]
                    check_data['first_name'] = name.split('<<')[1] if len(name.split('<<')) > 1 else ''
    if not check_number(data['fiscal_code']) and check_data:
        try:
            bd = translate_birth_date_to_code(check_data)
            last_name_abbrv = translate_name_to_abbreviation(check_data['last_name'])
            first_name_abbrv = translate_name_to_abbreviation(check_data['first_name'])
            data['fiscal_code'] = '{}{}{}{}'.format(last_name_abbrv, first_name_abbrv, bd, data['fiscal_code'][-5:])
        except Exception as e:
            print(str(e))
    return data
