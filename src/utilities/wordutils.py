import difflib
from nltk.corpus import wordnet as wn
import re

MATCH_TYPE_EQUAL = 0
MATCH_TYPE_IN = 1

RETURN_VALUE = 0
RETURN_MATCH = 1

def is_word(word):
    return len(wn.lemmas(word, lang="ita")) > 0

def clean(word, flag=None):
    word = word.replace("İ", "I")
    word = word.replace("Ž", "Z")
    word = word.replace("Ọ", "Q")

    if flag:
        word = word.lower().replace(flag.lower(), "")

    word = re.sub(r"[.,;:]", "", word, 0)

    return word.strip()


def find_flag(array, flag, not_flag=None):
    if not_flag:
        return next((x for x in array if flag.lower() in x.lower() and not_flag.lower() not in x.lower()), None)
    return next((x for x in array if flag.lower() in x.lower()), None)


def contains_flag(string, flag):
    return flag.lower() in string.lower()


def lookup_between_lists_by_flag(where, matches, flag, not_flag=None, clean_flag=True, match_type=MATCH_TYPE_EQUAL,
                                 to_return=RETURN_VALUE):
    found = find_flag(where, flag=flag, not_flag=not_flag)

    if found:
        if clean_flag:
            clean_value = clean(found, flag)
        else:
            clean_value = clean(found)

        if match_type == MATCH_TYPE_EQUAL:
            if any(clean_value.lower() == item.lower() for item in matches):
                return clean_value.upper()
        else:
            possible_values = []

            _matches = [item.lower() in clean_value.lower() for item in matches]

            if any(_matches):
                indexes = [i for i, x in enumerate(_matches) if x]

                matched = [matches[index] for index in indexes]

                for item in matched:
                    ratio = difflib.SequenceMatcher(None, clean_value.lower(), item).ratio()
                    if to_return == RETURN_VALUE:
                        possible_values.append({'ratio': ratio, 'value': item})
                    else:
                        possible_values.append({'ratio': ratio, 'value': clean_value.lower()})

            if len(possible_values) > 1:
                return sorted(possible_values, key=lambda k: k['ratio'], reverse=True)[0]['value'].upper()
            elif len(possible_values) > 0:
                return possible_values[0]['value'].upper()

    return None


def lookup_between_lists_by_match(where, matches, match_type=MATCH_TYPE_EQUAL, skip_length=None,
                                  to_return=RETURN_VALUE, skip_not_italian=False, skip_value=None):
    for value in where:
        clean_value = clean(value)

        if skip_length and len(clean_value) <= skip_length:
            continue

        if match_type == MATCH_TYPE_EQUAL:
            if skip_value:
                if any(clean_value.lower() == item.lower() and item.lower() != skip_value.lower() for item in matches):
                    return clean_value.upper()
            else:
                if any(clean_value.lower() == item.lower() for item in matches):
                    return clean_value.upper()
        else:
            possible_values = []

            if skip_value:
                _matches = [item.lower() != skip_value.lower() and item.lower() in clean_value.lower() for item in matches]
            else:
                _matches = [item.lower() in clean_value.lower() for item in matches]

            if any(_matches):
                if skip_not_italian:
                    words = value.split()

                    if not any(is_word(word) for word in words):
                        continue

                indexes = [i for i, x in enumerate(_matches) if x]

                matched = [matches[index] for index in indexes]

                for item in matched:
                    ratio = difflib.SequenceMatcher(None, value.lower(), item).ratio()
                    if to_return == RETURN_VALUE:
                        possible_values.append({'ratio': ratio, 'value': item})
                    else:
                        possible_values.append({'ratio': ratio, 'value': value.lower()})

            if len(possible_values) > 1:
                return sorted(possible_values, key=lambda k: k['ratio'], reverse=True)[0]['value']
            elif len(possible_values) > 0:
                return possible_values[0]['value']

    return None
