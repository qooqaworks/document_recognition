import cv2
import numpy as np
import os
import time
import random
from PIL import Image

# TIME_STAMP = int(time.time())

TIME_STAMP = 0

class ImagePreProcessor(object):

    # def processed_file_path_generator(self, file_path):
    #     file_name = os.path.basename(file_path)
    #     file_dir = os.path.dirname(file_path)
    #     file_name_with_ext = os.path.splitext(file_name)
    #
    #     file_path = '{}_{}.processed{}'.format(str(TIME_STAMP), file_name_with_ext[0],
    #                                              file_name_with_ext[1])
    #     return file_path

    def old_id_front(self, img_path):
        path_to_save = img_path
        img = cv2.imread(img_path)

        # best res: 7, 7 - gaussian | 75, 75, 75 - bilateral
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gauss = cv2.GaussianBlur(gray, (7, 7), 0)           # 7 7
        blurred = cv2.bilateralFilter(gauss, 75, 75, 75)    # 75 75 75

        _, threshold = cv2.threshold(blurred, 175, 255, cv2.THRESH_BINARY)      # 175 - front   # 200 - rear

        mask = threshold
        white_img = np.zeros(img.shape, img.dtype)
        white_img[:, :] = (255, 255, 255)
        white_mask = cv2.bitwise_and(white_img, white_img, mask=mask)
        masked_result = cv2.addWeighted(white_mask, 1, img, 1, 0, img)

        hsv = cv2.cvtColor(masked_result, cv2.COLOR_BGR2HSV)
        hsv_lower = np.array([0, 0, 0])
        hsv_upper = np.array([255, 255, 95])  # 95 - front, 125 - rear
        color_mask = cv2.inRange(hsv, hsv_lower, hsv_upper)
        res = cv2.bitwise_and(color_mask, color_mask, mask=color_mask)
        final_res = cv2.bitwise_not(res)
        cv2.imwrite(path_to_save, final_res)

        im = Image.open(path_to_save)
        im.save(path_to_save, dpi=(300, 300))
        im.close()
        return path_to_save

    def insurance_front(self, img_path):
        path_to_save = img_path
        img = cv2.imread(img_path)

        gauss = cv2.GaussianBlur(img, (5, 5), 0)            # 5 5
        blurred = cv2.bilateralFilter(gauss, 35, 75, 75)    # 35 75 75

        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        hsv_lower = np.array([0, 0, 0])
        hsv_upper = np.array([255, 255, 70])
        mask = cv2.inRange(hsv, hsv_lower, hsv_upper)
        masked_res = cv2.bitwise_and(mask, mask, mask=mask)
        final_res = cv2.bitwise_not(masked_res)

        cv2.imwrite(path_to_save, final_res)

        im = Image.open(path_to_save)
        im.save(path_to_save, dpi=(300, 300))
        im.close()
        return path_to_save

    def insurance_back(self, img_path):
        path_to_save = img_path
        img = cv2.imread(img_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gauss = cv2.GaussianBlur(gray, (5, 5), 0)
        blurred = cv2.bilateralFilter(gauss, 35, 75, 75)
        # _, thresh = cv2.threshold(blurred, 150, 255, cv2.THRESH_BINARY)

        im = Image.fromarray(blurred)
        im.save(path_to_save, dpi=(300, 300))
        im.close()
        return path_to_save

    def patent_front(self, img_path):
        path_to_save = img_path
        img = cv2.imread(img_path)

        # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # gauss = cv2.GaussianBlur(gray, (3, 3), 0)                # 3 3
        blurred = cv2.bilateralFilter(img, 75, 75, 75)        # 35 75 75

        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        hsv_lower = np.array([0, 0, 0])
        hsv_upper = np.array([255, 255, 200])                   # 100
        mask = cv2.inRange(hsv, hsv_lower, hsv_upper)
        masked_res = cv2.bitwise_and(mask, mask, mask=mask)
        final_res = cv2.bitwise_not(masked_res)

        final_res = cv2.GaussianBlur(final_res, (3, 3), 0)

        im = Image.fromarray(final_res)
        im.save(path_to_save, dpi=(300, 300))
        im.close()
        return path_to_save

    def new_id_back_test(self, img_path):
        path_to_save = img_path
        img = cv2.imread(img_path)

        # gauss = cv2.GaussianBlur(img, (3, 3), 0)
        # blurred = cv2.bilateralFilter(gauss, 35, 75, 75)
        #
        # gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)
        # _, threshold = cv2.threshold(blurred, 125, 255, cv2.THRESH_BINARY)
        # threshold = cv2.cvtColor(threshold, cv2.COLOR_BGR2GRAY)
        # _, threshold = cv2.threshold(threshold, 220, 255, cv2.THRESH_BINARY)
        #
        # # bgr = cv2.cvtColor(threshold, cv2.COLOR_GRAY2BGR)
        # # hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
        # # hsv_lower = np.array([0, 0, 0])
        # # hsv_upper = np.array([255, 255, 255])
        # # mask = cv2.inRange(hsv, hsv_lower, hsv_upper)
        #
        # cv2.imwrite(path_to_save, threshold)

        # img[:, :, 0] = 127
        # # img[:, :, 1] = 0
        # img[:, :, 2] = 255
        #
        # # hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        # # hsv_lower = np.array([0, 130, 240])
        # # hsv_upper = np.array([55, 255, 255])
        # # mask = cv2.inRange(hsv, hsv_lower, hsv_upper)
        # hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        # hsv_lower = np.array([0, 55, 240])
        # hsv_upper = np.array([165, 255, 255])
        # mask = cv2.inRange(hsv, hsv_lower, hsv_upper)
        #
        # img = cv2.bitwise_or(img, img, mask=mask)

        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        hsv_lower = np.array([0, 0, 0])
        hsv_upper = np.array([255, 255, 125])  # 150
        mask = cv2.inRange(hsv, hsv_lower, hsv_upper)

        img = cv2.bitwise_and(mask, mask, mask=mask)
        img = cv2.bitwise_not(img)

        cv2.imwrite(path_to_save, img)

        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # _, threshold = cv2.threshold(img, 110, 255, cv2.THRESH_BINARY)
        # img = cv2.cvtColor(threshold, cv2.COLOR_BGR2GRAY)
        # _, threshold = cv2.threshold(img, 175, 255, cv2.THRESH_BINARY)

        im = Image.open(path_to_save)
        im.save(path_to_save, dpi=(300, 300))
        im.close()
        return path_to_save

    def new_id_front(self, img_path):
        path_to_save = img_path
        img = cv2.imread(img_path)

        img[:, :, 0] = 0
        img[:, :, 2] = 0

        img = cv2.GaussianBlur(img, (3, 3), 0)

        _, threshold = cv2.threshold(img, 155, 255, cv2.THRESH_BINARY)

        cv2.imwrite(path_to_save, threshold)

        im = Image.open(path_to_save)
        im.save(path_to_save, dpi=(300, 300))
        im.close()
        return path_to_save

    # TODO: in progress
    def patent_back(self, img_path):
        path_to_save = img_path
        img = cv2.imread(img_path)

        gauss = cv2.GaussianBlur(img, (3, 3), 0)

        gray = cv2.cvtColor(gauss, cv2.COLOR_BGR2GRAY)
        _, threshold = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY_INV)

        kernel_v = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 25))
        kernel_h = cv2.getStructuringElement(cv2.MORPH_RECT, (35, 1))
        erode_v = cv2.erode(threshold, kernel_v, iterations=2)
        erode_h = cv2.erode(threshold, kernel_h, iterations=2)

        table = cv2.bitwise_or(erode_v, erode_h)
        table = cv2.bitwise_not(table)

        tbl_cntrs, hierarchy = cv2.findContours(table, cv2.RETR_LIST, 1)
        drawn_contours = cv2.drawContours(img, tbl_cntrs, -1, (0, 255, 255), 1, cv2.LINE_AA, hierarchy, 1)

        cv2.imwrite(path_to_save, drawn_contours)

        table_contours = list()
        img_clone = img.copy()
        img_w, img_h = img.shape[:2]
        img_area = img_w * img_h
        for contour in tbl_cntrs:
            x, y, w, h = cv2.boundingRect(contour)
            contour_area = w * h
            if contour_area < 0.01 * img_area and h > img_h * 0.015:
                table_contours.append({'x': x,
                                       'y': y,
                                       'w': w,
                                       'h': h,
                                       'contour': contour})
                cv2.rectangle(img_clone, (x, y), (x + w, y + h), (234, 135, 47), 2)

        table_contours = sorted(table_contours, key=lambda k: k['y'])
        i = 0
        table_contours_by_lines = list()
        for cnt in table_contours:
            if i:
                if table_contours_by_lines[-1][-1]['y'] < cnt['y'] + cnt['h'] / 2 < (table_contours_by_lines[-1][-1]['y'] + table_contours_by_lines[-1][-1]['h']):
                    table_contours_by_lines[-1].append(cnt)
                else:
                    table_contours_by_lines.append(list())
                    table_contours_by_lines[-1].append(cnt)
            else:
                table_contours_by_lines.append(list())
                table_contours_by_lines[-1].append(cnt)
            i += 1

        if not os.path.exists(path_to_save.rsplit('.', 1)[0]):
            os.makedirs(path_to_save.rsplit('.', 1)[0])
        file_name = os.path.basename(img_path)

        r = 1
        for line in table_contours_by_lines:
            c = 1
            for col in line:
                cropped_img = img.copy()[col['y']:col['y'] + col['h'], col['x']:col['x'] + col['w']]
                im = Image.fromarray(cropped_img)
                im.save('{}/{}_{}_{}.png'.format(path_to_save.rsplit('.', 1)[0], file_name, r, c), dpi=(300, 300))
                im.close()
                c += 1
            r += 1

        return path_to_save.rsplit('.', 1)[0]

    def old_id_back(self, img_path):
        path_to_save = img_path

        img = cv2.imread(img_path)

        img_height, img_width = img.shape[:2]
        cropped_img = img[0:img_height, 0:int(img_width / 2)]
        img_height, img_width = cropped_img.shape[:2]

        gauss = cv2.GaussianBlur(cropped_img, (5, 5), 0)
        blurred = cv2.bilateralFilter(gauss, 35, 75, 75)
        gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)
        _, threshold = cv2.threshold(gray, 175, 255, cv2.THRESH_BINARY)

        id_number_img = None
        # id_number_y = None
        img_clone = blurred.copy()
        contours, hierarchy = cv2.findContours(threshold, cv2.RETR_LIST, 1)
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            if img_height * 0.9 > h > 0.1 * img_height and img_width * 0.9 > w > img_width * 0.5 and y > img_height * 2 / 3:
                cv2.rectangle(img_clone, (x, y - int(h * 2 / 3)), (x + w, y), (0, 255, 0), 2)
                id_number_img = img_clone[y - int(h * 2 / 3):y, x:x + w]
                id_number_img = cv2.cvtColor(id_number_img, cv2.COLOR_BGR2RGB)
                # path_to_save_id_number = path_to_save.rsplit('.', 1)[0] + '_id_number.png'
                # id_number_y = y - int(h * 2 / 3)
                # cv2.imwrite(path_to_save_id_number, id_number_img)

        # cropped_img_mod = None
        # if id_number_img is not None and id_number_y is not None:
        #     cropped_img_mod = cropped_img[0:id_number_y, 0:img_width]
            # cv2.imwrite(path_to_save, cropped_img)

        # if cropped_img_mod is not None:
        #     gauss = cv2.GaussianBlur(cropped_img_mod, (3, 3), 0)
        #     blurred = cv2.bilateralFilter(gauss, 35, 75, 75)
        #     cv2.imwrite(path_to_save, blurred)

        im = Image.fromarray(id_number_img)
        im.save(path_to_save, dpi=(300, 300))
        im.close()
        return path_to_save

    def clear_image(self, documents):
        # print(documents)
        for document in documents:
            document_type = document["type"]
            document_path = document["path"]
            if document_type == "OLD_ID_BACK":
                document['path'] = self.old_id_back(document_path)
            elif document_type == "OLD_ID_FRONT":
                document['path'] = self.old_id_front(document_path)
            elif document_type == "H_INSURANCE_BACK":
                document['path'] = self.insurance_back(document_path)

            elif document_type == "NEW_ID_BACK":
                document['path'] = self.new_id_back_test(document_path)
            elif document_type == "PATENT_BACK":
                document['path'] = self.patent_back(document_path)
            elif document_type == "PATENT_FRONT":
                document['path'] = self.patent_front(document_path)
            elif document_type == "NEW_ID_FRONT":
                document['path'] = self.new_id_front(document_path)
            elif document_type == "H_INSURANCE_FRONT":
                document['path'] = self.insurance_front(document_path)

        return documents


def _back_test(dir_path='/Users/anton/Projects/Master_Group/document_recognition/tmp/h_insur_back'):
    pre = ImagePreProcessor()
    for entry in os.scandir(dir_path):
        if entry.name.endswith('.png') or entry.name.endswith('.jpg'):
            pre.insurance_back(entry.path)
