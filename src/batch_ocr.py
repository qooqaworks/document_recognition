import json
import os
from typing import List, Any

from google.cloud import vision
from google.oauth2 import service_account

from src.types.carta_identita_fronte import CartaIdentitaFronte
from src.types.carta_identita_retro import CartaIdentitaRetro
from src.types.patente import Patente
from src.types.request import Request
import pymysql.cursors

from src.types.tessera_sanitaria_retro import TesseraSanitariaRetro

#TODO: Switch from Google

class BatchOcr(object):
    requests = []
    documents: List[Request] = []

    def __init__(self):
        self.documents = []
        self.requests = []

        credentials = service_account.Credentials.from_service_account_file(
            os.path.join("/src", "json", "credentials.json"))

        self.client = vision.ImageAnnotatorClient(credentials=credentials)

        connection = pymysql.connect(host='localhost',
                                     user='user',
                                     password='password',
                                     db='db',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)

        try:
            with connection.cursor() as cursor:
                # Read a single record
                sql = "SELECT DISTINCT `nome` FROM `attivazioni`"
                cursor.execute(sql)
                result = cursor.fetchall()

                dbNames = [item['nome'].lower() for item in result]
        except:
            dbNames = []

        try:
            with connection.cursor() as cursor:
                sql = "SELECT DISTINCT `cognome` FROM `attivazioni`"
                cursor.execute(sql)
                result = cursor.fetchall()

                dbSurnames = [item['cognome'].lower() for item in result]
        except Exception as exc:
            dbSurnames = []

        try:
            with connection.cursor() as cursor:
                sql = "SELECT DISTINCT `toponimo` FROM `toponimi`"
                cursor.execute(sql)
                result = cursor.fetchall()

                dbToponimi = [item['toponimo'].lower() for item in result]
        except Exception as exc:
            dbToponimi = []
        finally:
            connection.close()

        with open(os.path.join("/src", "json", "nomi.json")) as file:
            localNames = json.loads(file.read())
        with open(os.path.join("/src", "json", "cognomi.json")) as file:
            localSurnames = json.loads(file.read())
        with open(os.path.join("/src", "json", "comuni.json")) as file:
            cities = json.loads(file.read())

        self.names = dbNames + list(set(localNames) - set(dbNames))
        self.surnames = dbSurnames + list(set(localSurnames) - set(dbSurnames))
        self.cities = cities
        self.toponimi = dbToponimi

    def add_request(self, request):
        """

        :type request: Request
        """

        if not any(doc.type == request.type for doc in self.documents):
            self.documents.append(request)
            self.requests.append(request.getRequest())

    def lookup(self):
        response = self.client.batch_annotate_images(self.requests)

        #print(response.responses[1])

        lookups = []

        for i, content in enumerate(response.responses):
            """
            :type content: ~google.cloud.vision_v1.types.BatchAnnotateImagesResponse
            """
            try:
                dati = content.text_annotations[0].description

                if self.documents[i].type == Request.PATENTE:
                    datiPatente = Patente(
                        content=dati,
                        names=self.names,
                        surnames=self.surnames,
                        cities=self.cities).lookup()

                    lookups.append({'tipo': Request.PATENTE, 'documento': 'PATENTE', 'dati': datiPatente})
                elif self.documents[i].type == Request.TESSERA_SANITARIA_RETRO:
                    datiTesseraSanitaria = TesseraSanitariaRetro(
                        content=dati,
                        names=self.names,
                        surnames=self.surnames,
                        cities=self.cities).lookup()

                    lookups.append({'tipo': Request.TESSERA_SANITARIA_RETRO, 'documento': 'TESSERA SANITARIA RETRO',
                                    'dati': datiTesseraSanitaria})
                elif self.documents[i].type == Request.CARTA_IDENTITA_FRONTE:
                    datiCI = CartaIdentitaFronte(
                        content=dati,
                        names=self.names,
                        surnames=self.surnames,
                        toponimi=self.toponimi,
                        cities=self.cities).lookup()

                    lookups.append(
                        {'tipo': Request.CARTA_IDENTITA_FRONTE, 'documento': 'CARTA IDENTITA FRONTE', 'dati': datiCI})
                elif self.documents[i].type == Request.CARTA_IDENTITA_RETRO:
                    datiCI = CartaIdentitaRetro(
                        content=dati).lookup()

                    lookups.append(
                        {'tipo': Request.CARTA_IDENTITA_RETRO, 'documento': 'CARTA IDENTITA RETRO', 'dati': datiCI})
            except Exception as exc:
                print(repr(exc))


        return lookups
