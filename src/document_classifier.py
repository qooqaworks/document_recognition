import os
import cv2
from src.scan_splitter import ScanSplitter
from copy import deepcopy
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class DocumentClassifier(object):

    def patent_front_check(self, document, height, width,  area):
        control = False
        gray = cv2.cvtColor(document, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(gray, 155, 255, cv2.THRESH_BINARY_INV)
        contours, _ = cv2.findContours(mask, 0, 1)
        contours = sorted(contours, key=lambda c: cv2.contourArea(c), reverse=True)
        rects = [cv2.boundingRect(cnt) for cnt in contours if 0.01 * area < cv2.contourArea(cnt) < 0.2 * area]
        for rect in rects:
            x, y, w, h = rect
            if 1.56 < w/h < 1.72 and 0.031 * area < w * h < 0.071 * area \
                    and 0 < x + w/2 < width/2 and 0 < y + h/2 < height/2:
                control = True
        return control

    def old_id_check(self, document, scan_area):
        control = False
        document_height = document.get().shape[0]
        document_width = document.get().shape[1]
        document_area = document_height * document_width
        if document_area / scan_area > 0.188:
            control = True

        return control

    def old_id_front_check(self, document, area, height, width):
        control = False
        net = cv2.dnn.readNetFromCaffe(
            BASE_DIR + "/src/utilities/deploy.prototxt.txt",
            BASE_DIR + "/src/utilities/res10_300x300_ssd_iter_140000.caffemodel")
        h, w, _ = document.get().shape
        blob = cv2.dnn.blobFromImage(cv2.resize(document, (300, 300)), 1.0,
                                     (300, 300), (104.0, 177.0, 123.0))
        net.setInput(blob)
        detections = net.forward()

        for i in range(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > 0.5:
                control = True

        return control

    def health_insurance_back_check(self, document, area, height):
        control = False
        # img_copy = deepcopy(document.get())
        gray = cv2.cvtColor(document, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(gray, 30, 255, cv2.THRESH_BINARY_INV)
        contours, _ = cv2.findContours(mask, 0, 1)
        contours = sorted(contours, key=lambda c: cv2.contourArea(c), reverse=True)
        rects = [cv2.boundingRect(cnt) for cnt in contours if 0.04 * area < cv2.contourArea(cnt) < 0.45 * area]
        for rect in rects:
            x, y, w, h = rect

            # cv2.rectangle(img_copy, (x, y), (x + w, y + h), (0, 255, 0), 4)

            if 6 < w/h < 9 and 0.15 * area < w * h < 0.27 * area \
                    and 0 < y + h/2 < height/2:

                control = True
        # cv2.imshow("img_copy", img_copy)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        return control

    def new_id_back_check(self,document, area):
        control = False
        gray = cv2.cvtColor(document, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(gray, 125, 255, cv2.THRESH_BINARY)
        gradX = cv2.Sobel(mask, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
        gradY = cv2.Sobel(mask, ddepth=cv2.CV_32F, dx=0, dy=1, ksize=-1)
        gradient = cv2.subtract(gradX, gradY)
        gradient = cv2.convertScaleAbs(gradient)
        blurred = cv2.blur(gradient, (5, 5))
        (_, thresh) = cv2.threshold(blurred, 95, 255, cv2.THRESH_BINARY)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 7))
        closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
        closed = cv2.erode(closed, kernel, iterations=4)
        closed = cv2.dilate(closed, kernel, iterations=4)
        contours, _ = cv2.findContours(closed, 0, 1)
        contours = sorted(contours, key=lambda c: cv2.contourArea(c), reverse=True)
        rects = [cv2.boundingRect(cnt) for cnt in contours if 0.04 * area < cv2.contourArea(cnt) < 0.35 * area]
        for rect in rects:
            x, y, w, h = rect
            if 6 < w/h < 9 and 0.075 * area < w * h < 0.165 * area:
                control = True

        return control

    def patent_back_check(self, document, width, height):
        control = False

        gray = cv2.cvtColor(document, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(gray, 145, 255, cv2.THRESH_BINARY_INV)

        kernel_vertical = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 35))
        kernel_horizontal = cv2.getStructuringElement(cv2.MORPH_RECT, (35, 1))

        vertical = cv2.erode(mask, kernel_vertical, iterations=2)
        horizontal = cv2.erode(mask, kernel_horizontal, iterations=2)

        vertical_contours, _ = cv2.findContours(vertical, 0, 1)
        horizontal_coutours, _ = cv2.findContours(horizontal, 0, 1)

        vertical_rects = [cv2.boundingRect(cnt) for cnt in vertical_contours if
                          cv2.boundingRect(cnt)[3] > 0.4 * height]
        horizontal_rects = [cv2.boundingRect(cnt) for cnt in horizontal_coutours if
                            cv2.boundingRect(cnt)[2] > 0.4 * width]

        # print("---- HORIZONTAL LINES ", len(horizontal_rects), "          VERTICAL LINES ", len(vertical_rects))
        if 2 < len(vertical_rects) < 4 and 12 < len(horizontal_rects) < 24:
            control = True

        return control

    def new_id_front_check(self, document):
        control = False
        net = cv2.dnn.readNetFromCaffe(
            BASE_DIR + "/src/utilities/deploy.prototxt.txt",
            BASE_DIR + "/src/utilities/res10_300x300_ssd_iter_140000.caffemodel")
        h, w, _ = document.get().shape
        blob = cv2.dnn.blobFromImage(cv2.resize(document, (300, 300)), 1.0,
                                     (300, 300), (104.0, 177.0, 123.0))
        net.setInput(blob)
        detections = net.forward()

        for i in range(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > 0.5:
                control = True
        return control

    def get_document_shapes(self, document):
        document_height = document.get().shape[0]
        document_width = document.get().shape[1]
        document_area = document_width * document_height
        return document_height, document_width, document_area

    def getDocumentContours(self, img):
        CANNY_THRESH_1 = 10
        CANNY_THRESH_2 = 100

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        edges = cv2.Canny(gray, CANNY_THRESH_1, CANNY_THRESH_2)
        edges = cv2.dilate(edges, None)
        edges = cv2.erode(edges, None)

        contours, _ = cv2.findContours(edges, 0, 1)
        contours = sorted(contours, key=lambda c: cv2.contourArea(c), reverse=True)

        return contours

    def detect(self, documents, scan_area):
        # print("DOCUMENTS: ", documents)
        classified_documents = list()
        for document in documents:
            # print(document)
            classified_document = dict()
            # classified_document['raw_data'] = document['raw_data']
            classified_document['type'] = "UNKNOWN"
            classified_document['path'] = document['path']
            document_height, document_width, _ = document['raw_data'].get().shape
            document_area = document_height * document_width
            # print("H, W, A: ", document_height, document_width, document_area)
            contours = self.getDocumentContours(document['raw_data'])
            if self.old_id_check(document['raw_data'], scan_area):
                classified_document['type'] = "OLD_ID_BACK"
                if self.old_id_front_check(document['raw_data'], document_area, document_height, document_width):
                    classified_document['type'] = "OLD_ID_FRONT"
                # splitter = ScanSplitter()
                # img = cv2.imread(document['path'])
                # document_img = splitter.white_background_clear(img, 70)
                # cv2.imwrite(document['path'], document_img)
            elif self.health_insurance_back_check(document['raw_data'], document_area, document_width):
                classified_document['type'] = "H_INSURANCE_BACK"
            elif self.new_id_back_check(document['raw_data'], document_area):
                classified_document['type'] = "NEW_ID_BACK"
            elif self.patent_back_check(document['raw_data'], document_width, document_height):
                classified_document['type'] = "PATENT_BACK"
            elif self.patent_front_check(document['raw_data'], document_height, document_width, document_area):
                classified_document['type'] = "PATENT_FRONT"
            elif self.new_id_front_check(document['raw_data']):
                classified_document['type'] = "NEW_ID_FRONT"
            else:
                classified_document['type'] = "H_INSURANCE_FRONT"
            classified_documents.append(classified_document)

        return classified_documents