import os
import subprocess
import cv2
from PIL import Image
import numpy as np
from copy import deepcopy

from colorsys import rgb_to_hsv

class ScanSplitter(object):

    def intersection(self, a, b, threshold=0):
        """
        Gets intersection result of 2 rectangles
        :param a: rectangle type tuple
        :type a: tuple
        :param b: rectangle type tuple
        :type b: tuple
        :param threshold: intersection threshold value
        :type threshold: int
        :return:  intersection rectangle
        :rtype: tuple
        """
        x = max(a[0], b[0])
        y = max(a[1], b[1])
        w = min(a[0] + a[2], b[0] + b[2]) - x
        h = min(a[1] + a[3], b[1] + b[3]) - y
        if w < threshold or h < threshold:
            return ()  # or (0,0,0,0) ?rota
        return (x, y, w, h)

    def getDocumentContours(self, img, CANNY_THRESH_1=10, CANNY_THRESH_2 = 100):

        drawn_img = deepcopy(img)

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


        edges = cv2.Canny(gray, CANNY_THRESH_1, CANNY_THRESH_2)
        edges = cv2.dilate(edges, None)
        edges = cv2.erode(edges, None)

        cv2.imwrite("/tmp/canny_thresh.png", edges)
        # cv2.imshow("gray", gray)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        contours, hierarchy = cv2.findContours(edges, cv2.RETR_EXTERNAL, 1)
        contours = sorted(contours, key=lambda c: cv2.contourArea(c), reverse=True)

        # cv2.drawContours(drawn_img, contours, -1, (0, 255, 255), 1, cv2.LINE_AA, hierarchy, 1)

        # cv2.imshow("drawn_img", drawn_img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        return contours

    def writeImage(self, image, tipo, folder):
        root_path = "/ocr_env/{}".format(folder)
        if not os.path.exists(root_path):
            os.makedirs(root_path)

        file = "{}/{}_{}.bmp".format(root_path, tipo['tipo'], tipo['verso'])

        cv2.imwrite(file, image)

    def white_background_clear(self, img, CANNY_THRESH=30):
        new_img = deepcopy(img)
        img_height = img.shape[0]
        img_width = img.shape[1]
        img_area = img_height * img_width
        contours = self.getDocumentContours(img, CANNY_THRESH_1=CANNY_THRESH)
        rects = [cv2.boundingRect(cnt) for cnt in contours if 0.3 * img_area < cv2.contourArea(cnt)]
        if rects:
            document_rect = rects[0]
            x, y, width, height = document_rect
            cutoutImage = img[y:y + height, x:x + width]
            new_img = deepcopy(cutoutImage)

        return new_img

    def process(self, path, id_operatore):
        # try:
        img = cv2.imread(path, 1)
        img_height = img.shape[0]
        img_width = img.shape[1]
        img_area = img_height * img_width
        documents = list()

        contours = self.getDocumentContours(img)
        # drawn_contours = cv2.drawContours(img, contours, -1, (0, 255, 255), 1)
        rects = [cv2.boundingRect(cnt) for cnt in contours if 0.01 * img_area < cv2.contourArea(cnt) < 0.9 * img_area]
        idx = 0

        # Remove inner contours
        inner_rects = list()
        if len(rects) > 2:
            for i in range(len(rects) - 1):
                for k in range(i + 1, len(rects)):
                    if self.intersection(rects[i], rects[k]):
                        # print(i, k)
                        min_rect = rects[i]
                        if (rects[k][2] * rects[k][3]) < (min_rect[2] * min_rect[3]):
                            min_rect = rects[k]
                        # print(min_rect)
                        inner_rects.append(min_rect)

        inner_rects = list(set(inner_rects))
        temp = list()
        # Filters outer rectangles
        for rect in rects:
            if rect not in inner_rects:
                temp.append(list(rect))
        rects = temp

        for rect in rects:
            document = dict()
            idx += 1
            x, y, width, height = rect
            tmp_folder = '/tmp/{}/'.format(id_operatore)
            tmp_file = tmp_folder + 'temp{}.png'.format(str(idx))
            # cv2.rectangle(drawn_img, (x, y), (x + width, y + height), (0, 255, 0), 5)

            # cv2.imshow("drawn_img", drawn_img)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            if not os.path.exists(tmp_folder):
                os.makedirs(tmp_folder)

            cutoutImage = img[y:y + height, x:x + width]

            curoutImage_rgb =  cv2.cvtColor(cutoutImage, cv2.COLOR_BGR2RGB)

            im = Image.fromarray(curoutImage_rgb)
            im.save(tmp_file, dpi=(300, 300))
            im.close()


            subprocess.call(
                'convert -density 300x300 -units PixelsPerInch "%s" -deskew 40%% "%s"' % (
                    tmp_file, tmp_file), shell=True, executable='/bin/bash')

            # print(tmp_file)

            deskewedImage = cv2.imread(tmp_file, 1)

            # cv2.imshow("BEFORE", deskewedImage)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            document_img= self.white_background_clear(deskewedImage)

            document_img_rgb = cv2.cvtColor(cutoutImage, cv2.COLOR_BGR2RGB)

            im = Image.fromarray(document_img_rgb)
            im.save(tmp_file, dpi=(300, 300))
            im.close()

            # cv2.imshow("AFTER", document_img)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            cv2.imwrite(tmp_file, document_img)

            document_img = cv2.UMat(document_img)

            document['raw_data'] = document_img
            document['path'] = tmp_file
            documents.append(document)


            # cv2.imshow("img", deskewedImage)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

        return documents, img_area

#/home/denny_k/Documents/tmp/ita_docs
