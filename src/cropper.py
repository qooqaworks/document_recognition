import os
import subprocess
from PIL import Image
import cv2
import numpy as np


def batch_deskew(dir_path):
    for entry in os.scandir(dir_path):
        if '.png' in entry.name:
            subprocess.call('convert -density 300 -units PixelsPerInch -deskew 40% "{0}" "{0}"'.format(entry.path), shell=True)

def crop_documents(img_path, id_operatore):
    documents = list()
    dir_to_save = '/tmp/{}/'.format(id_operatore)
    cropped_imgs_dir = dir_to_save
    if not os.path.exists(cropped_imgs_dir):
        os.makedirs(cropped_imgs_dir)
    img = cv2.imread(img_path)
    img_height, img_width = img.shape[:2]
    img_area = img_height * img_width
    img = img[int(img_height * 0.02):img_height - int(img_height * 0.02), int(img_width * 0.02):img_width - int(img_width * 0.02)]
    img_height, img_width = img.shape[:2]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 225, 255, cv2.THRESH_BINARY_INV)
    a = (1, 3)
    b = (3, 1)
    closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, a), iterations=5)
    closed = cv2.morphologyEx(closed, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, b), iterations=5)
    i = 0
    contours, hierarchy = cv2.findContours(closed, cv2.RETR_EXTERNAL, 1)
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        if w * h > img_height * img_width * 0.06:
            document = dict()
            document['path'] = str()
            document['coordinates'] = cv2.boundingRect(contour)
            cv2.rectangle(img.copy(), (x, y), (x + w, y + h), (0, 255, 0), thickness=4)
            cropped_img = img[y:y + h, x:x + w]
            cropped_save_path = '{0}{1}_{2}.png'.format(cropped_imgs_dir, img_path.rsplit('/', 1)[1].rsplit('.')[0], str(i))
            document['path'] = cropped_save_path
            cv2.imwrite(cropped_save_path, cropped_img)
            i += 1
            documents.append(document)
    # cv2.imwrite(dir_to_save + img_path.rsplit('/', 1)[1] + '.png', img)
    batch_deskew(cropped_imgs_dir)
    new_documents = list()
    for entry in os.scandir(cropped_imgs_dir):
        if '.png' in entry.name:
            for document in documents:
                if document['path'] == entry.path:
                    document['raw_data'] = None
                    cropped_img = cv2.imread(entry.path)
                    img_height, img_width = cropped_img.shape[:2]
                    gray = cv2.cvtColor(cropped_img, cv2.COLOR_BGR2GRAY)
                    _, thresh = cv2.threshold(gray, 225, 255, cv2.THRESH_BINARY_INV)
                    a = (1, 3)
                    b = (3, 1)
                    closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, a), iterations=5)
                    closed = cv2.morphologyEx(closed, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, b), iterations=5)
                    contours, hierarchy = cv2.findContours(closed, cv2.RETR_EXTERNAL, 1)
                    for contour in contours:
                        x, y, w, h = cv2.boundingRect(contour)
                        if w > img_width * 0.8 and h > img_height * 0.8:
                            cv2.rectangle(cropped_img.copy(), (x, y), (x + w, y + h), (0, 255, 0), thickness=4)
                            cropped_img_clean = cropped_img[y:y + h, x:x + w]
                            document['raw_data'] = cv2.UMat(cropped_img_clean)
                            cropped_img_clean_rgb = cv2.cvtColor(cropped_img_clean, cv2.COLOR_BGR2RGB)
                            im = Image.fromarray(cropped_img_clean_rgb)
                            im.save(entry.path, dpi=(300, 300))
                            im.close()
                    new_documents.append(document)

    print("HERE!!!!!!!!!!!!!!! ", len(new_documents))
    failed_old_id_candidates = list()
    documents = list()
    for document in new_documents:
        img_width, img_height = document['coordinates'][-2:]
        # w = 830, h = 1220, w/h = 0.6803
        print("for img ", document['path'], "w/h ratio is ", img_width/img_height)
        if 0.6 < img_width/img_height < 0.9:
            failed_old_id_candidates.append(document)
        else:
            documents.append(document)

    if len(failed_old_id_candidates) == 2:
        failed_old_id_candidates.sort(key=lambda x: x['coordinates'][0])
        left_side = failed_old_id_candidates[0]
        right_side = failed_old_id_candidates[1]
        left_side_img = cv2.imread(left_side['path'])
        right_side_img = cv2.imread(right_side['path'])
        new_img_full_width = int((left_side['coordinates'][2] + right_side['coordinates'][2]) * 1.1558)
        new_img_full_height = int(left_side['coordinates'][3] * 1.1017)
        offset = int(new_img_full_height * 0.046153)
        new_img = np.zeros((new_img_full_height, new_img_full_width, 3), np.uint8)
        new_img[::] = (255,255,255)
        new_img[offset:offset + left_side_img.shape[0], offset:offset + left_side_img.shape[1]] = left_side_img
        new_img[offset:offset + right_side_img.shape[0],
        3 * offset + left_side_img.shape[1]: 3 * offset + left_side_img.shape[1] + right_side_img.shape[
            1]] = right_side_img

        # cv2.imshow("img_copy", new_img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        subprocess.call('rm {}'.format(right_side['path']), shell=True, executable='/bin/bash')
        subprocess.call('rm {}'.format(left_side['path']), shell=True, executable='/bin/bash')

        cropped_img_clean_rgb = cv2.cvtColor(new_img, cv2.COLOR_BGR2RGB)
        im = Image.fromarray(cropped_img_clean_rgb)
        im.save(left_side['path'], dpi=(300, 300))
        im.close()

        merged_image = dict()
        merged_image['path'] = left_side['path']
        merged_image['raw_data'] = cv2.UMat(new_img)
        merged_image['coordinates'] = [left_side['coordinates'][0], left_side['coordinates'][1], new_img_full_width,
                                       new_img_full_height]
        documents.append(merged_image)


    # Full 1800x1300 | Half 900x1300, border 770x1180 (x1.17), distance between borders 120, width koef 1.1558, heigh koef 1.1017 | 60 offsets
    for document in documents:
        print(document['path'])
    return documents, img_area


# test_dict = dict(coordinates=[123,321,50,120])
# test_list = []
# test_list.append(test_dict)
# test_dict_2 = dict(coordinates=[234,432, 50, 120])
# test_list.append(test_dict_2)
# test_list.sort(key=lambda x: x['coordinates'][0], reverse=True)
