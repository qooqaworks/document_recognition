import datetime
import getopt
import logging
import os
import time
from pprint import pformat
import subprocess
import json
import argparse


import aiohttp_cors
from aiohttp import web

global logger

from src.cropper import crop_documents
from src.scan_splitter import ScanSplitter
from src.document_classifier import DocumentClassifier
from src.preprocessing import ImagePreProcessor
from parsers.parse_files import process_image


currentDate = datetime.datetime.now()
LOGS_PATH = "/logs/{}/{}/{}/".format(
        currentDate.year, currentDate.month, currentDate.day)
# TEST_CHAIN_IMAGES_DIR = "/home/denny_k/Documents/master_group_italy/123/"
# TEST_CHAIN_RESULTS_DIR = "/home/denny_k/Documents/master_group_italy/results/"

#
# def batch_chain():
#     for entry in os.scandir(TEST_CHAIN_IMAGES_DIR):
#         if entry.is_file() and not entry.name.endswith(".zip"):
#             test_chain(entry.path, "123456789" + entry.name.split(".")[0], TEST_CHAIN_RESULTS_DIR)
#
#
# def test_chain(img_path, id_operatore, results_path):
#     print(" \n---PROCESSING " + img_path + " ----------- \n")
#     subprocess.call('rm -rf {0}'.format("/tmp/" + id_operatore), shell=True)
#     start_time = time.time()
#     documents, scan_area = crop_documents(img_path, id_operatore)
#     # print(documents)
#     classifier = DocumentClassifier()
#     documents = classifier.detect(documents, scan_area)
#     preprocessor = ImagePreProcessor()
#     # print(documents)
#     documents = preprocessor.clear_image(documents)
#     documents = process_image(documents)
#     with open(results_path + id_operatore + "result.json", 'w') as f:
#         f.write(json.dumps(documents))
#     print("RESULTS: ", documents)
#     print(" --------------- PROCESSING TIME: ", time.time() - start_time,  " ------------------")


async def handle(request):
    logger.info("--------------------- PROCESSING POST REQUEST -------------------------")
    print("--------------------- PROCESSING POST REQUEST -------------------------")

    start_time = time.time()

    filename = "test.png"
    id_operatore = None

    currentDate = datetime.datetime.now()

    reader = await request.multipart()

    document_data = None
    while True:
        part = await reader.next()

        if part is None:
            break

        filedata = await part.read(decode=False)


        if part.name == 'scansione_fronte':
            filename = part.filename
            document_data = filedata
        elif part.name == 'scansione_retro':
            filename = part.filename
            document_data = filedata
        elif part.name == 'id_operatore':
            id_operatore = filedata.decode('utf-8')

    if not os.path.exists('/tmp/{}/'.format(id_operatore)):
        os.makedirs('/tmp/{}/'.format(id_operatore))

    document_path = os.path.join('/tmp/{}/'.format(id_operatore), 'scanned_documents.' + filename.split(".")[-1])
    documents = list()
    if document_path:

        try:
            with open(document_path, 'wb') as f:
                f.write(document_data)
        except TypeError:
            with open(document_path, 'w') as f:
                f.write(document_data)


        logger.info("--CROPPING DOCUMENTS")
        # print("--CROPPING DOCUMENTS")
        documents, scan_area = crop_documents(document_path, id_operatore)
        logger.info("----CLASSIFYING DOCUMENTS")
        # print("----CLASSIFYING DOCUMENTS")
        classifier = DocumentClassifier()
        documents = classifier.detect(documents, scan_area)
        logger.info("------PREPROCESSING DOCUMENTS")
        # print("------PREPROCESSING DOCUMENTS")
        preprocessor = ImagePreProcessor()
        documents = preprocessor.clear_image(documents)
        logger.info("--------OCRing AND PARSING RESULTS")
        # print("--------OCRing AND PARSING RESULTS")
        documents = process_image(documents)
        logger.info("----------RETURNING RESULTS")
        # print("----------RETURNING RESULTS")
    logger.info("-------------- PROCESSING TIME {} ------------------".format(time.time() - start_time))
    # print("-------------- PROCESSING TIME {} ------------------ \n\n".format(time.time() - start_time))

    return web.json_response(documents)


if __name__ == '__main__':

    ap = argparse.ArgumentParser()
    ap.add_argument("-t", "--testdenny", help="Enable this option if you are testing on denny_k PC")
    args = vars(ap.parse_args())

    if args['testdenny']:
        currentDate = datetime.datetime.now()
        LOGS_PATH = "/home/denny_k/Documents/master_group_italy/logs/{}/{}/{}/".format(
            currentDate.year, currentDate.month, currentDate.day)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    currentDate = datetime.datetime.now()
    logPath = LOGS_PATH

    if not os.path.exists(logPath):
        os.makedirs(logPath)

    handler = logging.FileHandler(logPath + 'server.log')
    handler.setLevel(logging.INFO)

    logger.addHandler(handler)

    # start(sys.argv[1:])
    app = web.Application()
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    cors.add(app.router.add_post('/ocr', handle))

    web.run_app(app, port=8040)
