import argparse
import subprocess
import time
import json
import os

from src.cropper import crop_documents
from src.document_classifier import DocumentClassifier
from src.preprocessing import ImagePreProcessor
from parsers.parse_files import process_image


def batch_chain(img_dir, results_dir):
    for entry in os.scandir(img_dir):
        if entry.is_file() and not entry.name.endswith(".zip"):
            test_chain(entry.path, "123456789" + entry.name.split(".")[0], results_dir)


def test_chain(img_path, id_operatore, results_path):
    print(" \n---PROCESSING " + img_path + " ----------- \n")
    subprocess.call('rm -rf {0}'.format("/tmp/" + id_operatore), shell=True)
    start_time = time.time()
    documents, scan_area = crop_documents(img_path, id_operatore)
    # print(documents)
    classifier = DocumentClassifier()
    documents = classifier.detect(documents, scan_area)
    preprocessor = ImagePreProcessor()
    # print(documents)
    documents = preprocessor.clear_image(documents)
    documents = process_image(documents)
    with open(results_path + id_operatore + "result.json", 'w') as f:
        f.write(json.dumps(documents))
    print("RESULTS: ", documents)
    print(" --------------- PROCESSING TIME: ", time.time() - start_time,  " ------------------")

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--images", required=True, help="path to images directory")
    ap.add_argument("-o", "--output", required=True, help="path to results output directory")
    args = vars(ap.parse_args())


    batch_chain(args['images'], args['output'])